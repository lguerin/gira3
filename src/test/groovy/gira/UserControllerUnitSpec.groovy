package gira

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Shared
import spock.lang.Specification

class UserControllerUnitSpec extends Specification implements ControllerUnitTest<UserController>, DomainUnitTest<User> {

    @Shared
    User user

    def setup() {
        user = Fixtures.buildUser()
    }

    def cleanup() {
    }

    void "should say hello to current user"() {
        setup:
        controller.userDataService = Stub(UserDataService) {
            getUser(_) >> user
        }

        when:
        controller.params.id = user.id
        controller.hello()

        then:
        status == 200
        response.text == 'Hello Guillaume Laforge'
    }
}