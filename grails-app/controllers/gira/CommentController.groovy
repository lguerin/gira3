package gira

import grails.rest.*
import grails.converters.*

class CommentController extends RestfulController {

    static responseFormats = ['json', 'xml']

    CommentController() {
        super(Comment)
    }

    @Override
    protected Comment createResource() {
        Comment instance = resource.newInstance()
        bindData instance, getObjectToBind()
        UserStory us = UserStory.get(params.userStoryId)
        instance.userStory = us
        instance
    }

    @Override
    protected List listAllResources(Map params) {
        UserStory us = UserStory.get(params.userStoryId)
        return Comment.getAllCommentsByUserStoryId(us, params)
    }
}
