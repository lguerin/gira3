package gira

import gira.exceptions.BusinessException
import gira.exceptions.TechnicalException
import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import org.springframework.transaction.annotation.Propagation

@Service(UserStory)
abstract class UserStoryDataService {

    protected  abstract UserStory getUserStory(Serializable id)

    protected  abstract List<UserStory> findUserStories(Map args)

    @Transactional(noRollbackFor = BusinessException)
    boolean updateStoryPoints(Serializable id, int points) {
        log.info "Set user story from updateStoryPoints: $id points: $points"
        UserStory us = this.getUserStory(id)
        println "Version: " + us.version
        if (us != null) {
            us.storyPoints = points
            if (points < 0) {
                throw new TechnicalException("User story points must be positive")
            }
            if (points < 10) {
                throw new BusinessException("User story points must be greater or equal to 10")
            }
            return true
        }
        return false
    }

    @Transactional
    UserStory updateTestTransaction(Serializable id, int storyPoints = 5) {
        UserStory us = this.getUserStory(id)

        // Update title
        us.title = 'xxx'
        us.save()
        int oldStoryPoints = us.storyPoints

        try {
            this.updateStoryPoints(id, storyPoints)
        }
        catch (BusinessException e) {
            log.error "Fail to update story points"
            us.storyPoints = oldStoryPoints
        }
        return us
    }
}
