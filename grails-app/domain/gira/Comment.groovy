package gira

import gira.types.WorkflowState
import groovy.time.TimeCategory
import org.hibernate.sql.JoinType

class Comment implements Comparable<Comment> {

    String description
    User createdBy
    Date dateCreated = new Date()

    static belongsTo = [userStory: UserStory]

    static constraints = {
    }

    static List<Comment> getAllCommentsByUserStoryId(UserStory userStory, Map params) {
        return Comment.executeQuery("from Comment c where c.userStory = :us",
            [us: userStory], params)
    }

    @Override
    int compareTo(Comment c) {
        return c.dateCreated.compareTo(this.dateCreated)
    }

    static List<Comment> lastMinutesComments(int minutes, WorkflowState state) {
        Date dateAfter, dateBefore
        use (TimeCategory) {
            dateAfter = new Date()
            dateBefore = dateAfter - minutes.minutes
        }
        return Comment.createCriteria().list() {
            createAlias "userStory", "us", JoinType.INNER_JOIN
            eq "us.state", state
            between "dateCreated", dateBefore, dateAfter
        }
    }
}
