package gira

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification

@Integration
@Rollback
class UserSpec extends Specification {

    User user

    def setup() {
        user = Fixtures.buildUser()
    }

    def cleanup() {
    }

    void "should have some existing users"() {
        expect:
        User.count() >= 1
    }

    void "should not create a new user on duplicated login"() {
        setup: "Given an existing user"
        def count = User.count()

        when: "I create a new user with an already existing login"
        Fixtures.buildUser([login: user.login])

        then: "New user is not added"
        User.count() == count
    }
}
