package gira

import grails.gorm.services.Service
import grails.gorm.services.Where
import grails.gorm.transactions.Transactional

@Service(User)
interface UserDataService {

    User getUser(Serializable id)

    List<User> findAll()

    @Transactional
    @Where({ email ==~ pattern })
    void delete(String pattern)

}

