package gira

import org.springframework.beans.factory.annotation.Value


class UserInterceptor {

    @Value('${gira.user.timer.enabled:false}')
    private Boolean enabled

    private static final String START_TIME = 'ACTION_START_TIME'

    boolean before() {
        if (!enabled) {
            return true
        }

        request[START_TIME] = System.currentTimeMillis()
        return true
    }

    boolean after() { true }

    void afterView() {
        if (!enabled) {
            return
        }

        Long start = request[START_TIME] as Long
        Long end = System.currentTimeMillis()

        log.info "Total time of User action '$actionName': ${end - start} ms"
    }
}
