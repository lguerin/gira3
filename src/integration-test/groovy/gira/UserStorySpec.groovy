package gira

import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import spock.lang.Specification

@Integration
@Rollback
class UserStorySpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "should get list of user strory comments"() {
        expect: "List of comments"
            UserStory.get(1).comments.size() > 0
    }

    void "should get ordered list of user story comments by dateCreated desc"() {
        when:
        def comments = UserStory.get(1).comments

        then:
        comments.size() >= 3
        comments[0].dateCreated > comments[1].dateCreated
        comments[1].dateCreated > comments[2].dateCreated
    }

    void "should list users that follows more than one user story"() {
        setup:
        User user1 = Fixtures.buildUser()
        User user2 = Fixtures.buildUser()
        UserStory us1 = Fixtures.buildUserStory()
        UserStory us2 = Fixtures.buildUserStory()
        us1.addToFollowers(user1).addToFollowers(user2).save(flush: true)
        us2.addToFollowers(user2).save(flush: true)

        when:
        List<User> list = UserStory.listUsersFollowingMoreThanOneUserStory()

        then:
        list.size() >= 1
        list*.login.contains(user2.login)

        cleanup:
        us1.delete(flush: true)
        us2.delete(flush: true)
    }

    void "should sum story points of active user stories assigned to a given user"() {
        setup:
        User user = UserStory.get(1).assignedTo
        UserStory us = Fixtures.buildUserStory([assignedTo: user, storyPoints: 10])
        List<UserStory> userStories = UserStory.findAllByAssignedTo(user)

        when:
        int sum = UserStory.getActiveStoryPointsSum(user).get()

        then:
        sum == userStories.sum { it.storyPoints }

        cleanup:
        us.delete(flush: true)
    }
}
