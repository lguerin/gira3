package gira

import gira.types.WorkflowState
import grails.plugins.rest.client.RestBuilder
import grails.plugins.rest.client.RestResponse
import grails.testing.mixin.integration.Integration
import grails.transaction.Rollback
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.beans.factory.annotation.Value
import spock.lang.Shared
import spock.lang.Specification

@Integration
@Rollback
class GiraRestApiSpec extends Specification {

    @Shared
    RestBuilder rest = new RestBuilder()

    /**
     * Server port configuration for Grails test
     * environment is set to server.port = 0, which
     * means a random available port is used each time
     * the application starts.
     * The value for the port number is accessible
     * via ${local.server.port} in our integration test.
     */
    @Value('${local.server.port}')
    Integer serverPort

    void "should get list of user stories"() {
        when:
        RestResponse response = this.get("/api/userStories")
        JSONArray list = new JSONArray(response.json)

        then:
        response.status == 200
        list?.size() >= 10
        list[0].getAt("storyPoints") >= 1
        list[0].getAt("state") == WorkflowState.TO_TEST.name()
    }

    void "should get a user story from its id"() {
        when:
        RestResponse response = this.get("/api/userStories/1")
        JSONObject us = new JSONObject(response.json)

        then:
        response.status == 200
        us != null
        us.createdBy != null
        us.commentsCount > 0
        us.followersCount > 0
    }

    void "should get all comments of a given user story"() {
        when:
        RestResponse response = this.get("/api/userStories/1/comments")
        JSONArray list = new JSONArray(response.json)

        then:
        response.status == 200
        list?.size() >= 3
    }

    void "should get first comment of a given user story"() {
        when:
        RestResponse response = this.get("/api/userStories/1/comments/1")
        JSONObject comment = new JSONObject(response.json)

        then:
        response.status == 200
        comment.description != null
        comment.userFullname != null
        comment.userId != null
    }

    void "should add a new comment to a given user story"() {
        setup:
        UserStory us = UserStory.get(1)
        List<User> users = User.findAll()
        String userLogin = users[0].login
        int commentsCount = us.comments?.size()

        when:
        String req = """
            {
                "description": "One more comment",
                "createdBy": "$userLogin"
            }
        """
        RestResponse response = this.post("/api/userStories/1/comments", req)

        then:
        response.status == 201
    }

    void "should delete a comment of a given user story"() {
        setup:
        UserStory us = UserStory.get(1)
        int commentsCount = us.comments?.size()

        when:
        RestResponse response = this.delete("/api/userStories/1/comments/1")
        us = UserStory.get(1)

        then:
        response.status == 204
    }

    void "should get followers of a given user story"() {
        setup:
        UserStory us = UserStory.get(1)

        when:
        RestResponse response = this.get("/api/userStories/1/followers")
        JSONArray list = new JSONArray(response.json)

        then:
        response.status == 200
        list.size() == us.followers.size()
    }

    void "should update story points of a given user story"() {
        when:
        RestResponse response = this.put("/api/userStories/1/storyPoints/20")
        UserStory us = UserStory.get(1)

        then:
        response.status == 204
        us.storyPoints == 20
    }

    void "should get last user stories"() {
        when:
        RestResponse response = this.get("/api/userStories/last?max=2")
        JSONArray list = new JSONArray(response.json)

        then:
        response.status == 200
        list.size() == 2

        when:
        response = this.get("/api/userStories/last?max=20")
        list = new JSONArray(response.json)

        then:
        response.status == 200
        list.size() == 20
    }

    private RestResponse get(String path) {
        println "GET $path"
        RestResponse response = rest.get("http://localhost:${serverPort}${path}") {
            accept('application/json')
        }
        println ">> $response.json"
        return response
    }

    private RestResponse post(String path, String req = "") {
        println "POST: $path, request: $req"
        def response = rest.post("http://localhost:${serverPort}${path}") {
            contentType "application/json"
            json req
        }
        println ">> $response.json"
        return response
    }

    private RestResponse put(String path, String req = "") {
        println req ? "PUT: $path, request: $req" : "PUT: $path"
        def response = rest.put("http://localhost:${serverPort}${path}") {
            contentType "application/json"
            if (req) {
                json req
            }
        }
        println ">> $response.json"
        return response
    }

    private RestResponse delete(String path) {
        println "DELETE $path"
        RestResponse response = rest.delete("http://localhost:${serverPort}${path}") {
            accept('application/json')
        }
        return response
    }
}