package gira.exceptions

class TechnicalException extends RuntimeException {

    public TechnicalException(String msg) {
        super(msg)
    }

    public TechnicalException(String msg, Throwable cause) {
        super(msg, cause)
    }
}
