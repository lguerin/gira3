package gira

import gira.types.WorkflowState
import grails.compiler.GrailsCompileStatic

@GrailsCompileStatic
class UserStory {

    String title
    String description
    User assignedTo
    User createdBy
    int storyPoints = 0
    WorkflowState state = WorkflowState.NEW
    SortedSet comments
    Date dateCreated = new Date()

    static hasMany = [comments: Comment, followers: User]

    static mappedBy = [assignedTo: "none"]

    static constraints = {
        assignedTo nullable: true
        followers nullable: true
        storyPoints min: 0, max: 100
    }

    static mapping = {
        followers joinTable: [name: 'USER_STORY_FOLLOWERS']
    }

    static List<User> listUsersFollowingMoreThanOneUserStory() {
        return UserStory.executeQuery("select u from UserStory us join us.followers u group by u having count(*) > 1")
    }

    static namedQueries = {
        getActiveStoryPointsSum { User u ->
            eq "assignedTo", u
            inList "state", [WorkflowState.NEW, WorkflowState.IN_PROGRESS, WorkflowState.TO_TEST]
            projections {
                sum "storyPoints"
            }
        }
    }
}
